import React from 'react';

import { Route, Switch } from 'react-router-dom';
import styled from 'styled-components';

import HomeContainer from './homeContainer';
import DashboardContainer from './dashboard';
import PurchaseContainer from './purchaseContainer';
import TmpAuthContainer from './tmpAuthContainer';

const ContentRouter = () => (
  <Content>
    <Switch>
      <Route path="/" exact component={HomeContainer} />
      <Route path="/dashboard" component={DashboardContainer} />
      <Route path="/purchase/new" component={PurchaseContainer} />
      <Route path="/auth" component={TmpAuthContainer} />
    </Switch>
  </Content>
);

const Content = styled.div`
  margin-top: 1.25em;
  padding: 0.625em;
`;

export { ContentRouter as default };
