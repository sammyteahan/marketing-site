import React from 'react';

import styled from 'styled-components';

class TmpAuthContainer extends React.Component {
  render() {
    return (
      <div>
        <h1>Sign in via Linked in</h1>
        <div>
          <br />
          <p>
            This should simulate the auth flow. If the correct local storage
            values have been set (<CodeSnippet>redirect-purchase-form</CodeSnippet>
            and <CodeSnippet>purchase-form-values</CodeSnippet>) then clicking on
            dashboard in the top nav should now go to the correct component with
            the correct values shown.
          </p>
          <br />
          <p>
            This should only work once directly after authenticating. The dashboard
            should clean up after itself if the above local storage values are set
            so it doesn't affect users who are immediately concerned with signing in.
          </p>
        </div>
      </div>
    );
  }
}

const CodeSnippet = styled.span`
  border-radius: 4px;
  padding: 2px 4px;
  background-color: #948e8e;
`;

export { TmpAuthContainer as default };
