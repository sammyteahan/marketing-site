import React from "react";

import { FiClipboard } from "react-icons/fi";

import localStorage from '../utils/localStorage';

class Dashboard extends React.Component {
  state = {
    showPurchaseScreen: false,
    showReceipt: false,
    savedFormValues: {},
  };

  componentDidMount() {
    const shouldRedirectToPurchaseForm = localStorage.shouldRedirectToPurchaseForm();

    if (shouldRedirectToPurchaseForm) {
      const localStorageFormValues = localStorage.loadPurchaseFormState();
      console.log(localStorageFormValues);
      this.setState({ savedFormValues: localStorageFormValues, showPurchaseScreen: true });
    }
  }

  /**
   * clean up after ourselves so that client doesn't see
   * the purchase screen each time they come to the dashboard.
   *
   * An alternative solution would be to use componentWillUnmount
   * by itself or in conjunction with this method.
   */
  handlePurchaseCancel = () => {
    localStorage.clearRedirects();
    localStorage.clearPurchaseFormState();
    this.setState({ showPurchaseScreen: false });
  }

  onClick = () => {
    this.setState({ showPurchaseScreen: false });
    localStorage.setFormStatus();
  };

  renderPlans() {
    return (
      <div className="row">
        <div className="col-md-12 col-lg-12">
          <div className="box plan">
            <h4 className="title">Your career plans</h4>
            <div className="row justify-content-center">
              <div className="col-lg-3 col-md-4 col-sm-6 mb-4">
                <div className="card h-100">
                  <FiClipboard />
                  <div className="card-body">
                    <h4 className="card-title">Marketing project manager</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderSuggestedBooks() {
    return (
      <div className="row" style={{ marginTop: 20 }}>
        <div className="col-md-12 col-lg-12">
          <div className="box">
            <h4 className="title">Suggested Books</h4>
            <div className="row justify-content-center">
              <div className="col-lg-3 col-m4-4 col-sm-6 mb-4">
                <div className="card h-100">
                  <FiClipboard />
                  <div className="card-title">
                    <h4>The title of this book is this</h4>
                    <p className="card-text">
                      You should read this book becuase
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderSuggestedCourses() {
    return (
      <div className="row" style={{ marginTop: 20 }}>
        <div className="col-md-12 col-lg-12">
          <div className="box">
            <h4 className="title">Suggested courses</h4>
          </div>
        </div>
      </div>
    );
  }

  renderContent() {
    /**
     * if we hit this first guard clause, it means that the user has just
     * come from the purchase form -> sign in flow and should be shown
     * appropriate purchase options (at least as I understand it)
     */
    if (this.state.showPurchaseScreen) {
      const { savedFormValues: { email, report } } = this.state;

      return (
        <section id="dashboard2" className="section-bg">
          <div className="container">
            <h3>Cart</h3>
            <br />
            <pre>
              form values:
              <br />
              email: {email}
              <br />
              job title: {report}
            </pre>
            <br />
            <div>
              <p>
                Thank you for purchasing your report. It will be available
                within 48 hours and you will be notified via email when it
                is ready
              </p>
            </div>
            <div>
              <button onClick={this.handlePurchaseCancel}>Cancel</button>
            </div>
          </div>
        </section>
      );
    }

    return (
      <section id="dashboard2" className="section-bg">
        <div className="container">
          <header className="section-header">
            <h3>Dashboard</h3>
          </header>
          {this.renderPlans()}
          {this.renderSuggestedBooks()}
          {this.renderSuggestedCourses()}
        </div>
      </section>
    );
  }

  render() {
    return (
      <div>
        {this.renderContent()}
      </div>
    );
  }
}

export { Dashboard as default };
