import React from "react";

import { reduxForm } from "redux-form";

import PurchaseForm from "../components/purchaseForm";
import PurchaseFormReview from "../components/purchaseReview";

class PurchaseContainer extends React.Component {
  state = { showFormReview: false };

  handleFormSubmit = () => {
    this.setState({ showFormReview: true });
  }

  handleCancel = () => {
    this.setState({ showFormReview: false });
  }

  renderContent() {
    if (this.state.showFormReview) {
      return (
        <PurchaseFormReview onCancel={this.handleCancel} />
      );
    }

    return <PurchaseForm onPurchaseSubmit={this.handleFormSubmit} />;
  }

  render() {
    return (
      <div>
        {this.renderContent()}
      </div>
    );
  }
}

export default reduxForm({
  form: 'surveyForm',
})(PurchaseContainer);
