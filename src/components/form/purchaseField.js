import React from "react";

import { FaEnvelope } from "react-icons/fa";

const PurchaseField = ({ input, label, meta: { error, touched } }) => (
  <div>
    <i><FaEnvelope /></i>
    <label>{label}</label>
    <input {...input} style={{ marginBottom: 5 }} />
    <div>
      {touched && error}
    </div>
  </div>
)

export { PurchaseField as default };
