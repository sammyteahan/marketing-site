/**
 * Purchase form fields
 */
const purchaseFields = [
  {
    label: 'Email',
    name: 'email',
    noValueError: 'Please provide a your email'
  },
  {
    label: 'Desired Job Title',
    name: 'report',
    noValueError: 'Please provide a job title'
  },
];

export { purchaseFields as default };
