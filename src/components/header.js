import React from 'react';

import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Header = () => (
  <HeaderContainer>
    <LogoContainer>
      <HeaderLink to="/">
        <Title>Marketing site</Title>
      </HeaderLink>
    </LogoContainer>
    <NavContainer>
      <LinkWrapper to="/dashboard">Dashboard</LinkWrapper>
      <LinkWrapper to="/purchase/new">Purchase Plan</LinkWrapper>
    </NavContainer>
  </HeaderContainer>
);

const HeaderContainer = styled.header`
  background: steelblue;
  display: flex;
  align-items: stretch;
  height: 50px;
  color: white;
`;

const LogoContainer = styled.div`
  display: flex;
  flex-grow: 1;
  align-items: center;
  padding: 0 10px;
`;

const Title = styled.h2`
  font-weight: 400;
`;

const NavContainer = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row-reverse;
  flex-grow: 4;
`;

const LinkWrapper = styled(Link)`
  text-decoration: none;
  color: #fff;
  margin: 0 25px;
  cursor: pointer;
`;

const HeaderLink = styled(Link)`
  text-decoration: none;
  color: #fff;
  cursor: pointer;
`;

export { Header as default };
