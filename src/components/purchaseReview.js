import React from 'react';

import { map, get } from 'lodash';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import localStorage from '../utils/localStorage';
import purchaseFormFields from './form/purchaseFields';

const PurchaseFormReview = ({ onCancel, formValues, submitForm, history, ...rest }) => {
  const handleReviewSubmit = () => {
    localStorage.savePurchaseFormState(formValues);
    localStorage.setPurchaseFormRedirect();

    /**
     * @NOTE I don't have fully fledged out redux actions,
     * or auth wired up, so I'm going to simulate
     * going through an oauth flow by opening up an '/auth'
     * route in a new tab. This should simulate what a hard
     * GET request would do. Which would remove any state
     * stored in a component level, which should force us to
     * pull values out of local storage
     */
    return simulateAuthFlow('http://localhost:3000/auth');
  };

  const simulateAuthFlow = url => {
    const _window = window.open(url, '_blank');
    _window.focus();
  };

  const renderReviewFields = () => {
    return (
      map(purchaseFormFields, ({ name, label }) => (
        <div key={name}>
          <label>{label}</label>
          <div>
            {get(formValues, name)}
          </div>
        </div>
      ))
    )
  };

  return (
    <div className="myDiv">
      <h5>Please confirm your entries</h5>
      {renderReviewFields()}
      <br />
      <p>
        We use LinkedIn to help us generate our reports, so you will now be
        directed to LinkedIn to login and grant us permission to see your work
        history.
      </p>
      <button className="yellow darken-3 btn-flat" onClick={onCancel}>Back</button>
      <SubmitButton onClick={handleReviewSubmit}>Submit</SubmitButton>
    </div>
  );
};

const SubmitButton = styled.button`
  border: 1px solid;
  padding: 5px 10px;
  margin: 0 10px;
`;

function mapStateToProps(state) {
  return { formValues: state.form.purchaseForm.values };
}

export default connect(
  mapStateToProps,
)(withRouter(PurchaseFormReview));
