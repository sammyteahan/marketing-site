import React, { Component } from "react";

import { map } from 'lodash';
import styled from 'styled-components';
import { reduxForm, Field } from "redux-form";
import { Link } from "react-router-dom";

import PurchaseField from './form/purchaseField';
import purchaseFormFields from './form/purchaseFields';

class PurchaseForm extends Component {
  renderFields() {
    return (
      map(purchaseFormFields, ({ label, name }) => (
        <Field
          key={name}
          component={PurchaseField}
          type="text"
          label={label}
          name={name}
        />
      ))
    );
  }

  render() {
    return (
      <section>
        <div className="myDiv">
          <h4>Select your Report Packages</h4>
          <div className="md-form mb-5">
            <form
              onSubmit={this.props.handleSubmit(() => this.props.onPurchaseSubmit())}
            >
              {this.renderFields()}
              <Link to="/dashboard" className="red btn-flat white-text">Cancel</Link>
              <div style={{ margin: 10, display: 'inline-block' }} />
              <SubmitButton
                type="submit"
                className="teal btn-flat right white-text"
              >
                Next
              </SubmitButton>
            </form>
          </div>
        </div>
      </section>
    )
  }
}

const SubmitButton = styled.button`
  border: 1px solid;
  padding: 5px 10px;
`;

function validate(values) {
  const errors = {};

  if (!values.email) {
    errors.email = 'Email is required';
  }

  if (!values.report) {
      errors.report = 'Desired job title is required';
  }

  return errors;
}

export default reduxForm({
  validate,
  form: 'purchaseForm',
  destroyOnUnmount: false,
})(PurchaseForm);
