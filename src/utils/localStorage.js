/**
* @desc simple utility object to facilitate
* the use of localStorage throughout the application.
*/

const REDIRECT_TO_PURCHASE_FORM = 'redirect-purchase-form';
const FORM_VALUES_KEY = 'purchase-form-values';

const util = {
  store: typeof window !== 'undefined' ? window.localStorage : {},
  redirectKey: REDIRECT_TO_PURCHASE_FORM,
  purchaseFormValues: FORM_VALUES_KEY,

  savePurchaseFormState(formObject) {
    try {
      const serializedFormState = JSON.stringify(formObject);
      this.store.setItem(this.purchaseFormValues, serializedFormState);
    } catch (error) {
      console.error(`Error serializing state ${error}`);
    }
  },

  loadPurchaseFormState() {
    try {
      const serializedState = this.store.getItem(this.purchaseFormValues);

      if (serializedState === null) {
        return undefined;
      }

      return JSON.parse(serializedState);
    } catch (err) {
      return undefined;
    }
  },

  setPurchaseFormRedirect() {
    this.store.setItem(REDIRECT_TO_PURCHASE_FORM, true);
  },

  shouldRedirectToPurchaseForm() {
    return this.store.getItem(REDIRECT_TO_PURCHASE_FORM);
  },

  clearRedirects() {
    this.store.removeItem(REDIRECT_TO_PURCHASE_FORM);
  },

  clearPurchaseFormState() {
    this.store.removeItem(this.purchaseFormValues);
  },
};

export { util as default };
