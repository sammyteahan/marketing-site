import React from 'react';

import styled from 'styled-components';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import store from './store';
import Header from './components/header';
import ContentRouter from './containers/contentRouter';

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Header />
      <ContentWrapper>
        <ContentRouter />
      </ContentWrapper>
    </BrowserRouter>
  </Provider>
);

const ContentWrapper = styled.div`
  margin: auto;
  max-width: 800px;

  @media screen and (max-width: 800px) {
    padding: 0 10px;
  }
`;

export default App;
