###### Getting started

```
$> git clone https://gitlab.com/sammyteahan/marketing-site.git
$> cd marketing-site
$> yarn # install dependencies
$> yarn start # start app
```
